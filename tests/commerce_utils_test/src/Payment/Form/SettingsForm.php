<?php

namespace Drupal\commerce_utils_test\Payment\Form;

use Commerce\Utils\Payment\Form\SettingsFormBase;

/**
 * {@inheritdoc}
 */
class SettingsForm extends SettingsFormBase {

  /**
   * {@inheritdoc}
   */
  public function form(array &$form, array &$settings) {
  }

}
